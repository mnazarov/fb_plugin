//
//  sociallibrary.m
//  sociallibrary
//
//  Created by User on 11.10.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "sociallibrary.h"
#import "Accounts/ACAccount.h"
#import "Accounts/ACAccountType.h"
#import "Accounts/ACAccountStore.h"
#import "Social/SLRequest.h"

@implementation sociallibrary
@end

NSString* fb_appid;

ACAccountStore *store;
ACAccount *fb_account = nil;

typedef void(^FBAccessGrantedHandler)();

NSString* CreateNSString (const char* string)
{
  if (string)
  {
    return [NSString stringWithUTF8String: string];
  }
  else
  {
    return [NSString stringWithUTF8String: ""];
  }
}

  void _fb_post_impl(NSString *text)
  { 
    ACAccountType *fb_account_type = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSArray *accounts = [store accountsWithAccountType:fb_account_type];
    fb_account = [accounts objectAtIndex:0];
    SLRequest *fb_request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                               requestMethod:SLRequestMethodPOST
                                                         URL:[NSURL URLWithString:@"https://graph.facebook.com/me/feed"]
                                                  parameters:[NSDictionary dictionaryWithObject:text forKey:@"message"]];
    [fb_request setAccount:fb_account];
    [fb_request performRequestWithHandler:^(NSData* responseData, NSHTTPURLResponse* urlResponse, NSError* error)
    {
      NSLog([[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding]);
    }];
  }
    
void _fb_request_access(NSArray *permissions, FBAccessGrantedHandler handler)
{
    ACAccountType *fb_account_type = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSDictionary *dict = @{ACFacebookAppIdKey : fb_appid, ACFacebookPermissionsKey : permissions, ACFacebookAudienceKey : ACFacebookAudienceEveryone};
    [store requestAccessToAccountsWithType:fb_account_type
                                   options:dict
                                completion:^(BOOL granted, NSError *error)
     {
         if (granted && error == nil)
         {
             handler();
         }
     }];
}

extern "C"
{
  void _fb_init(const char* appid) 
  {
      if (store == nil) store = [[ACAccountStore alloc] init];
      fb_appid = CreateNSString(appid);
  }

  void _fb_post(const char* text)
  {
    NSString* post = CreateNSString(text);
    if (fb_account != nil)
    {
      return _fb_post_impl(post);
    }
    
    _fb_request_access(@[@"email"], ^()
                       {
                         _fb_request_access(@[@"read_stream"], ^()
                                            {
                                              _fb_request_access(@[@"publish_stream"], ^()
                                                                 {
                                                                     _fb_post_impl(post);
                                                                 });
                                            });
                       });
  } // _fb_post
} // extern "C"
